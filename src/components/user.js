import React, {Component} from 'react';

export default class DataInfo extends Component {
  handleClick = event => {
    this.user.style.textDecoration = 'line-through';
  };

  render() {
    const {label, value} = this.props;
    return (
      <span className="post__content" onClick={this.handleClick}>
        <b>{label}</b>
        <span ref={user => (this.user = user)}>{value}</span>
      </span>
    );
  }
}
