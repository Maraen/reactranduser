import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import Post from './user';

const ReactRoot = document.getElementById('react-root');

const load = async () => {
  const response = await axios.get('https://randomuser.me/api');
  const {data} = response;
  console.log(data);

  const packagedData = {
    key: data.results[0].name.last,
    picture: data.results[0].picture.large,
    firstname: data.results[0].name.first,
    lastname: data.results[0].name.last,
    uname: data.results[0].login.username,
    age: data.results[0].dob.age,
    email: data.results[0].email,
    telephone: data.results[0].phone,
  };

  // ReactDOM.render(<Post />);
  // j'ai rater ma solution, je me dit qu'il y a un moyen de fair plus propre avec un autre composant.

  ReactDOM.render(
      <div className="post">
        <div className="post__head">
          <img picture={packagedData.picture} src={packagedData.picture} />
          <h1 name={packagedData.firstname}>
            {packagedData.firstname} {packagedData.lastname}
          </h1>
        </div>
        <Post label="Username:" value={packagedData.uname} />
        <Post label="Age:" value={packagedData.age} />
        <Post label="Email:" value={packagedData.email} />
        <Post label="Phone:" value={packagedData.telephone} />
      </div>,
      ReactRoot
  );
};

export default load;
