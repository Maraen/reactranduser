import React from 'react';
import ReactDOM from 'react-dom';
import Component from './components/props';
import './styles/global.scss';

const ReactRoot = document.getElementById('react-root');

const app = (
  <div>
    <Component />
  </div>
);

ReactDOM.render(app, ReactRoot);
